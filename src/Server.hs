{-# LANGUAGE OverloadedStrings #-}

module Server where

import qualified Web.Scotty as S
import Network.HTTP.Types
import Control.Monad.IO.Class
import Control.Monad

import Database.Persist

import IncomingEmail
import OutgoingEmail
import Listhost
import Postmark
import DataAccess

server = do
  S.post "/email" $ do
    incomingEmail <- S.jsonData :: S.ActionM IncomingMessage
    toSend <- liftIO $ routeEmail incomingEmail
    liftIO . sequence_ $ map sendMessage toSend
    S.status ok200
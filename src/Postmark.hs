module Postmark where

import Network.Curl
import qualified Data.ByteString.Lazy.Char8 as ByteString
import Data.Aeson

import OutgoingEmail
import Settings

{-|
  sendMessage uses libcurl to go out to Postmark and send an OutgoingMessage.
  it relies on the value postmarkToken from src/Settings.hs (excluded from Git)
-}
sendMessage :: OutgoingMessage -> IO ()
sendMessage message = do
  curl <- initialize
  _ <- do_curl_ curl postmarkEndpoint curlOptions :: IO CurlResponse
  return ()

 where
  postmarkEndpoint = "https://api.postmarkapp.com/email"
  curlOptions = [CurlPost True
                , CurlHttpHeaders ["X-Postmark-Server-Token: " ++ postmarkToken
                                  , "Content-Type: application/json"
                                  , "Accept: application/json"]
                , CurlPostFields [ByteString.unpack $ encode message]]
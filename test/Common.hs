{-# LANGUAGE OverloadedStrings #-}
module Common where

import Database.Persist
import Database.Persist.Sqlite
import Database.Persist.TH

import Prelude hiding (catch)
import System.Directory
import Control.Exception
import System.IO.Error hiding (catch)
import System.Environment
import Data.Maybe
import Control.Monad.IO.Class

import DataAccess
import IncomingEmail

listDomain = "lists.example.com"

resetTestDB = do
  removeIfExists "ephemeral-test-db.sqlite3"
  setEnv "LIST_DB" "ephemeral-test-db.sqlite3"
  withDatabase $ runMigration migrateAll  

personFixtures = withDatabase $ do
  insert Person { personName = "test-person-1", personEmail = "test-1@example.com" }
  insert Person { personName = "test-person-2", personEmail = "test-2@example.com" }
  insert Person { personName = "test-person-3", personEmail = "test-3@example.com" }
  insert Person { personName = "test-person-4", personEmail = "test-4@example.com" }

  return ()

listhostFixtures = withDatabase $ do
  insert (Listhost "test-listhost-1")
  insert (Listhost "test-listhost-2")
  return ()

membershipFixtures = withDatabase $ do
  insert Membership { membershipPersonId = toSqlKey 1, membershipListhostId = toSqlKey 1, membershipCanSend = True }
  insert Membership { membershipPersonId = toSqlKey 2, membershipListhostId = toSqlKey 1, membershipCanSend = False }
  insert Membership { membershipPersonId = toSqlKey 3, membershipListhostId = toSqlKey 1, membershipCanSend = False }

  insert Membership { membershipPersonId = toSqlKey 1, membershipListhostId = toSqlKey 2, membershipCanSend = False }
  insert Membership { membershipPersonId = toSqlKey 2, membershipListhostId = toSqlKey 2, membershipCanSend = False }
  insert Membership { membershipPersonId = toSqlKey 3, membershipListhostId = toSqlKey 2, membershipCanSend = True }


  return ()

withSampleIncomingEmails computation = computation [ IncomingMessage { 
                                                      from_ = "test-1@example.com"
                                                    , to_ = "test-listhost-1@lists.example.com"
                                                    , subject_ = "Test Subject 1"
                                                    , body_ = "Test Body 1."
                                                  }, IncomingMessage {
                                                      from_ = "test-2@example.com"
                                                    , to_ = "test-listhost-1@lists.example.com"
                                                    , subject_ = "[test-listhost-1] Test Subject 1"
                                                    , body_ = "Test Body 2."
                                                  },IncomingMessage {
                                                      from_ = "test-3@example.com"
                                                    , to_ = "test-listhost-2@lists.example.com"
                                                    , subject_ = "[test-listhost-2] Test Subject 1"
                                                    , body_ = "Test Body 3."
                                                  }
                                                  ]

withTwoPeople computation = do
  (p1, p2) <- withDatabase $ do
    person1 <- liftIO $ findPerson "test-1@example.com"
    person2 <- liftIO $ findPerson "test-2@example.com"
    return (fromJust person1, fromJust person2)
  computation (p1, p2)

withList computation = do
  list <- withDatabase $ liftIO $ findList "test-listhost-1"
  computation (fromJust list)

-- |Stackoverflow: http://stackoverflow.com/questions/8502201/remove-file-if-it-exists-in-haskell
removeIfExists :: FilePath -> IO ()
removeIfExists fileName = removeFile fileName `catch` handleExists
  where handleExists e
          | isDoesNotExistError e = return ()
          | otherwise = throwIO e
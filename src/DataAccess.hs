{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module DataAccess where

import Database.Persist
import Database.Persist.Sqlite
import Database.Persist.TH
import Database.Persist.Quasi
import qualified Database.Esqueleto as E
import Database.Esqueleto ((^.))

import qualified Data.Text as Text
import System.Posix.Env

share [mkPersist sqlSettings, mkMigrate "migrateAll"] $(persistFileWith lowerCaseSettings "src/schema")

withDatabase computation = do
  db <- getEnvDefault "LIST_DB" "listserv.sqlite3"
  runSqlite (Text.pack db) computation

-- |What it sounds like.
findList :: String -> IO (Maybe (Entity Listhost))
findList list = withDatabase $
  selectFirst [ListhostName ==. list] []

findPerson :: String -> IO (Maybe (Entity Person))
findPerson email = withDatabase $
  selectFirst [PersonEmail ==. email] []

{-|
  membersOfList performs a SQL query using the esqueleto DSL to 
  leverage the join table in the schema and produce a list of the people
  who are on a list.
-}
membersOfList :: Entity Listhost -> IO [Entity Person]
membersOfList list = withDatabase $
  E.select $ E.from $ \(l `E.InnerJoin` p `E.InnerJoin` m) -> do
    E.on (m ^. MembershipListhostId E.==. l ^. ListhostId)
    E.on (m ^. MembershipPersonId E.==. p ^. PersonId)
    E.where_ $ l ^. ListhostId E.==. E.valkey (fromSqlKey . entityKey $ list)
    return p

-- |Determine if someone is an authorized sender for the given list.
authorizedSender :: Entity Person -> Entity Listhost -> IO Bool
authorizedSender person list = withDatabase $ do
  membership <- selectFirst [MembershipPersonId ==. entityKey person, MembershipListhostId ==. entityKey list] []
  case membership of
    Just m -> return $ membershipCanSend . entityVal $ m
    Nothing -> return False

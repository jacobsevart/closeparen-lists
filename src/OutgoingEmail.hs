{-# LANGUAGE OverloadedStrings #-}

module OutgoingEmail where

import Data.Aeson
import qualified Data.Text as Text

-- |Type intended to be JSON serialized and POSTed to Postmark.
data OutgoingMessage = OutgoingMessage { from :: Text.Text
  , to :: Text.Text
  , subject :: Text.Text
  , body :: Text.Text
} deriving Show

instance ToJSON OutgoingMessage where
  toJSON (OutgoingMessage f t s b) = object [ 
        "From" .= f
        , "To" .= t
        , "Subject" .= s
        , "TextBody" .= b]

module DataAccessSpec (spec) where

import Test.Hspec
import Test.HUnit
import Common

import Database.Persist
import Database.Persist.Sqlite
import Data.Maybe

import DataAccess

spec = describe "data access functions" $ before_ 
  (resetTestDB >> personFixtures >> listhostFixtures >> membershipFixtures) $ do
  describe "findList" $ do
    it "finds an existing list" $ withList $ \l -> do
      (listhostName . entityVal $ l) `shouldBe` "test-listhost-1"

    it "doesn't find a non-existent list" $ do
      l <- findList "some other list"
      isNothing l `shouldBe` True

  describe "findPerson" $ do
    it "finds an existing person" $ do
      p <- findPerson "test-1@example.com"
      (personName . entityVal . fromJust $ p) `shouldBe` "test-person-1"

    it "doesn't find a non-existing person" $ do
      p <- findPerson "test@example.coom"
      isNothing p `shouldBe` True

  describe "membersOfList" $
    it "finds members of a list" $ withList $ \list -> do
      members <- membersOfList list
      map (personName . entityVal) members `shouldBe` ["test-person-1", "test-person-2", "test-person-3"]

  describe "authorizedSender" $
    it "correctly identifies authorized senders" $ 
      withList $ \list -> withTwoPeople $ \(person1, person2) -> do
      person1_authorized <- authorizedSender person1 list
      person1_authorized `shouldBe` True

      person2_authorized <- authorizedSender person2 list
      person2_authorized `shouldBe` False




{-# LANGUAGE OverloadedStrings #-}

module OutgoingEmailSpec (spec) where

import Test.Hspec
import Test.HUnit
import Common

import System.IO
import Data.Aeson
import Data.Maybe

import OutgoingEmail

import qualified Data.ByteString.Lazy as B

spec = describe "incoming email parsing" $
  it "should output a correctly formatted JSON email" $ do
    let mail = OutgoingMessage {
        from = "sender@example.com"
      , to = "receiver@example.com"
      , subject = "Test"
      , body = "Hello"
    }

    sampleOutMail <- B.readFile "test/sampleOutgoingEmail.json"
    let sample = decode sampleOutMail :: Maybe Value
    let ourOutput = decode (encode mail) :: Maybe Value

    sample `shouldBe` ourOutput





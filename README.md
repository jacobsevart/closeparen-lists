# Closeparen-lists

The beginnings (and core) of a better list-host system.

# Setting up

## Prerequisites
Besides what Cabal will take of for you, you will need

1. A domain (probably a subdomain) to dedicate to the installation
2. A Postmark server set up to receive email on that domain
3. Action from Postmark to support to enable a blanket "Sender Signature" for all addresses @ your inbound domain.
4. To expose the http endpoint /email on the public internet, and configure it as your Inbound endpoint in Postmark's admin panel.

## Configuration
Create src/Settings.hs as a module that exports two variables:

postmarkToken = "your-postmark-server-token-here"

listDomain = "your-postmark-inbound-domain-here.tld"

## Building
cabal sandbox init

cabal install --only-dependencies --enable-tests

cabal build

## Testing
cabal test

## Running the server
cabal run

# Functionality
## No admin panel
As yet, there is no configuration interface. I recommend sqlitebrowser for Mac :). There are 3 tables: person, listhost, and a join table called membership. 

## Permissions
The join table entry for a (user_id, listhost_id) pair specifies whether the user is allowed to send mail on that list: can_send must == 1 for sending to work. Otherwise, the sender receives a reply indicating that his message will not be delivered.

## Sending email
A user who has a membership on a list (with can_send == 1) sends an email to listname@list-domain.tld. Postmark makes a POST to /email; routeEmail does the heavy lifting of coming up with a list of emails to send in response, and sendEmail POSTs them all to Postmark, from where they are sent to list members.

The list currently rewrites the subject line to include a [listname] tag if it does not exist. All messages are sent "From" the listname@list-domain.tld, but the human name in the "From" header is set to the name on file for the message sender.
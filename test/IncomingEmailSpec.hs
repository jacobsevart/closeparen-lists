{-# LANGUAGE OverloadedStrings #-}


module IncomingEmailSpec (spec) where

import Test.Hspec
import Test.HUnit
import Common

import System.IO
import Data.Aeson
import Data.Maybe
import qualified Data.ByteString.Lazy as B

import IncomingEmail

spec = describe "incoming email parsing" $
  it "should handle a correctly formatted JSON email" $ do
    sampleJson <- B.readFile "test/sampleIncomingEmail.json"
    let decoded = decode sampleJson :: Maybe IncomingMessage
    from_ (fromJust decoded) `shouldBe` "myUser@theirDomain.com"
    to_ (fromJust decoded) `shouldBe` "451d9b70cf9364d23ff6f9d51d870251569e+ahoy@inbound.postmarkapp.com"
    subject_ (fromJust decoded) `shouldBe` "This is an inbound message"
    body_ (fromJust decoded) `shouldBe` "[ASCII]"



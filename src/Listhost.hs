{-# LANGUAGE OverloadedStrings #-}

module Listhost where

import qualified Data.Text as T
import Database.Persist

import IncomingEmail
import OutgoingEmail
import DataAccess
import Settings

{-| 
  routeEmail implements the core user story: email comes in, email goes out.
  It checks if the list exists, if the sender exists, and if the sender
  is authorized for the list. If any of these checks fail, a bounce is 
  generated. If they pass, the message is transformed by transformEmail
  for each recipient on the list except the sender, who receives a special
  case of bounce implemented by the function "success." This function performs
  database IO but does not send any email; it produces a list intended for 
  consumption by sendMessage.
-}
routeEmail :: IncomingMessage -> IO [OutgoingMessage]
routeEmail message = do
  sender <- findPerson (T.unpack . from_ $ message)
  list <- findList listName
  case (list, sender) of
    (Just l, Just s) -> do
      authorized <- authorizedSender s l
      case authorized of
        True -> do
          members <- membersOfList l
          return (success (subject_ message) (from_ message) : 
            map (transformEmail message listName s) (filter isNotSender members)) where
              isNotSender m = entityKey m /= entityKey s

        False -> return [bounce (from_ message)]

    (_, _) -> return [bounce (from_ message)]
  where
    listName = takeWhile (/= '@') $ T.unpack . to_ $ message

{-|
  transformEmail inserts the list name in square brackets into the subject line,
  rewrites the from and to fields (the message is sent from the list but with 
  the originating user's name from the database), and passes along the body
  unaltered.
-}
transformEmail :: IncomingMessage -> String -> Entity Person -> Entity Person -> OutgoingMessage 
transformEmail message listName sender recipient = OutgoingMessage { 
    from = T.pack $ (personName . entityVal $ sender) ++ " <" ++ (T.unpack . to_ $ message) ++ ">"
  , to = T.pack . personEmail . entityVal $ recipient
  , subject = taggedSubject (subject_ message) listName
  , body = body_ message
} where
  taggedSubject :: T.Text -> String -> T.Text
  taggedSubject subject listName
    | tag `T.isInfixOf` subject = subject
    | otherwise = tag `T.append` subject
    where
      tag = T.pack $ "[" ++ listName ++ "] "

{-|
  bounce is a generic error message which informs the user that his message
  is not going to be delivered. For simplicity and security, "no such list,"
  "no such person," and "person is not authorized to send" are collapsed into
  a single error.
-}
bounce ::  T.Text -> OutgoingMessage
bounce dest = OutgoingMessage {
    from = T.pack $ "Listhost System <postmaster@" ++ listDomain ++ ">"
  , to = dest
  , subject = "I couldn't send your last message"
  , body = "Sorry, I couldn't find a list by that name for which you are an authorized sender."
}

{-|
  success is an email reply acknowledging the system's intent to deliver
  messages to a list.
-}
success :: T.Text -> T.Text -> OutgoingMessage
success subj dest = OutgoingMessage {
    from = T.pack $ "Listserv System <postmaster@" ++ listDomain ++ ">"
  , to = dest
  , subject = "Message '" `T.append` subj `T.append` "' distributed"
  , body = "Your message has been distributed."
}


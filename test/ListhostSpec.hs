{-# LANGUAGE OverloadedStrings #-}

module ListhostSpec (spec) where

import Test.Hspec
import Test.HUnit
import Common

import Database.Persist
import Database.Persist.Sqlite
import Data.Maybe
import qualified Data.Text as T

import DataAccess
import Listhost
import IncomingEmail
import OutgoingEmail

spec = describe "Listhosts" $ do

  describe "routeEmail" $ do
    it "should bounce unknown sender" $
      withSampleIncomingEmails $ \emails -> do
        let strangerEmail = (head emails) { from_ = "joe@example.com" }
        response <- routeEmail strangerEmail
        subject (head response) `shouldSatisfy` ("couldn't send" `T.isInfixOf`)

    it "should bounce unknown list" $
      withSampleIncomingEmails $ \emails -> do
        let weirdListEmail = (head emails) { to_ = "random@lists.example.com" }
        response <- routeEmail weirdListEmail
        subject (head response) `shouldSatisfy` ("couldn't send" `T.isInfixOf`)

    describe "with fixtures" $ before_ (resetTestDB >> personFixtures >> listhostFixtures >> membershipFixtures) $ do
      it "should bounce known sender known list unauthorized" $
        withSampleIncomingEmails $ \emails -> do
          response <- routeEmail (emails !! 1)
          subject (head response) `shouldSatisfy` ("couldn't send" `T.isInfixOf`)

      it "should acknowledge known sender known list authorized" $
         withSampleIncomingEmails $ \emails -> do
          response <- routeEmail (head emails)
          subject (head response) `shouldSatisfy` ("distributed" `T.isInfixOf`)

      it "should not send you your own message" $
        withSampleIncomingEmails $ \emails -> do
          response <- routeEmail (head emails)

          any (\x -> to x == "test-1@example.com" && body x == "Test Body 1.") response `shouldBe` False

      it "should send others on the list your message" $
        withSampleIncomingEmails $ \emails -> do
          response <- routeEmail (emails !! 2)

          length (filter (\x -> body x == "Test Body 3.") response) `shouldBe` 2
          map to response `shouldSatisfy` ("test-1@example.com" `elem`)
          map to response `shouldSatisfy` ("test-2@example.com" `elem`)


  describe "transformEmail" $ before_ (resetTestDB >> personFixtures) $ do
    it "should tag the subject if no tag present" $ 
      withSampleIncomingEmails $ \emails -> withTwoPeople $ \(person1,person2) -> do

        let xformed = transformEmail (head emails) "test-listhost-1" person1 person2
        subject xformed `shouldSatisfy` ("[test-listhost-1]" `T.isInfixOf`)

    it "should not tag twice if tag already present" $ 
      withSampleIncomingEmails $ \emails -> withTwoPeople $ \(person1,person2) -> do

        let xformed = transformEmail (emails !! 1) "test-listhost-2" person1 person2
        T.count "[test-listhost-2]" (subject xformed) `shouldBe` 1

    it "should set the sender correctly" $ 
      withSampleIncomingEmails $ \emails -> withTwoPeople $ \(person1,person2) -> do

        let xformed = transformEmail (head emails) "test-listhost-1" person1 person2
        from xformed `shouldBe` "test-person-1 <test-listhost-1@lists.example.com>"

    it "should set the recipient correctly" $
      withSampleIncomingEmails $ \emails -> withTwoPeople $ \(person1,person2) -> do

        let xformed = transformEmail (emails !! 1) "tests-listhost-2" person1 person2
        to xformed `shouldBe` "test-2@example.com"

    it "should not alter the body" $
      withSampleIncomingEmails $ \emails -> withTwoPeople $ \(person1,person2) -> do

        let xformed = transformEmail (emails !! 1) "tests-listhost-2" person1 person2

        body xformed `shouldBe` "Test Body 2."

  describe "bounce" $ do
    it "should be addressed according to its argument" $ do
      let b = bounce "bob@xample.com"
      to b `shouldBe` "bob@xample.com"
      from b `shouldSatisfy` ("postmaster" `T.isInfixOf`)
    it "should be apologetic" $ do
      let b = bounce "bob@example.coom"
      body b `shouldSatisfy` ("Sorry" `T.isInfixOf`)
      subject b `shouldSatisfy` ("couldn't send your last message" `T.isInfixOf`)

  describe "success" $ do
    it "should be addressed according to its argument" $ do
      let b = success "test message" "bob@xample.com"
      to b `shouldBe` "bob@xample.com"
      from b `shouldSatisfy` ("postmaster" `T.isInfixOf`)
    it "should describe success" $ do
      let b = success "test message" "bob@example.com"
      body b `shouldSatisfy` ("has been distributed" `T.isInfixOf`)
      subject b `shouldSatisfy` ("distributed" `T.isInfixOf`)
      subject b `shouldSatisfy` ("test message" `T.isInfixOf`)


module Main where

import Web.Scotty
import Network.Wai.Middleware.RequestLogger
import Server
import DataAccess

import Database.Persist
import Database.Persist.Sqlite
import Database.Persist.TH

{-|
  Run the Postmark API endpoint on port 3000.
  By default, works out of configuration from listserv.sqlite3.
  Set the LIST_DB environment variable to another file to use
  a different sqlite3 database, or redefine "withDatabase" in
  DataAccess.hs to use a different backend.
-}
main :: IO ()
main = do
  withDatabase $ runMigration migrateAll
  scotty 3000 $ do 
    middleware logStdout
    server
{-# LANGUAGE OverloadedStrings #-}

module IncomingEmail where

import Data.Aeson
import Network.Curl
import qualified Data.Text as Text
import qualified Data.ByteString.Lazy.Char8 as ByteString
import Control.Monad

-- | Type intended to be populated by inbound emails from Postmark.
data IncomingMessage = IncomingMessage { to_ :: Text.Text
  , from_ :: Text.Text
  , subject_ :: Text.Text
  , body_ :: Text.Text
}


-- Note: it looks like this will break if the list is not the first recipient.
-- Will need to scan all recipients for one matching our domain, consider that as list name
-- Also consider multiple lists as recipients. Create virtual copies of the email? 
-- What will postmark do? Hopefully send multiple callbacks
instance FromJSON IncomingMessage where
  parseJSON (Object v) = IncomingMessage <$> ((v .: "ToFull") >>= (\ff -> head ff .: "Email")) <*> 
              ((v .: "FromFull") >>= (.: "Email")) <*> v.: "Subject" <*> v.: "TextBody"  
  parseJSON _ = mzero